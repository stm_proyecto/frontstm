import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as global from './variablesGlobales';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }) 
  };

@Injectable({
    providedIn: 'root'
  })

export class SalidaEfectivoService {

    constructor(private http: HttpClient) { }

    consultarSalidaEfectivo(): Observable<any> {
        return this.http.get( global.urlServidor + '/salidaEfectivo/listaSalidasEfectivo');
      }

      agregarSalidaEfectivo(salidaEfectivo): Observable<any> {
        return this.http.post( global.urlServidor + '/salidaEfectivo/guardarSalidasEfectivo', salidaEfectivo, httpOptions);
      } 

      borraSalidaEfectivo(salidaEfectivo): Observable<any> {
        return this.http.post( global.urlServidor + '/salidaEfectivo/borrarSalidasEfectivo', salidaEfectivo , httpOptions);
      } 

}