import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Empleado } from "./model/Empleado"
import { Servicio } from "./model/Servicio"
import { AreaServicio } from "./model/AreaServicio"

@Injectable({
  providedIn: 'root'
})
export class AsignarservicioService {

  urlServidor = 'http://localhost:8081/stm/';
  url;

  constructor( 
      protected httpStm: HttpClient ) {
  }

  public ListarEmpleado(body: any) {
    return this.httpStm.get<Empleado[]>(this.urlServidor + `empleados`, body);
  }

  public ListarServicio(body: any) {
    return this.httpStm.get<Servicio[]>(this.urlServidor + `servicios`, body);
  }

  public ListarAreaServicio(body: any) {
    return this.httpStm.get<AreaServicio[]>(this.urlServidor + `areas-servicios`, body);
  }

  public guardarTurno(body: any) {
    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.httpStm.post(this.urlServidor + `turnos`, body, { headers });
  }
}
