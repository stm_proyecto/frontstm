import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as global from './variablesGlobales';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
 
};

@Injectable({
  providedIn: 'root'
})
export class TarifasService{

  constructor(private http: HttpClient) { }


  register(user): Observable<any> {
    return this.http.post( global.urlServidor + '/tarifa/registro',user, httpOptions);
  }

  
  delete(id): Observable<any> {
    return this.http.post( global.urlServidor + '/tarifa/borrarTarifa/?id=' + id, httpOptions);
  }

  listaTarifa(user): Observable<any> {
    return this.http.get( global.urlServidor + '/tarifa/listaTarifa',user);
  }

  listaTarifas(): Observable<any> {
    return this.http.get( global.urlServidor + '/tarifa/listaTarifa');

    
  }

  buscaEspeciales(): Observable<any> {
    return this.http.get( global.urlServidor + '/tarifa/buscaEspeciales');
 
  }

  
}
