
export interface Tarifa {
    tipoTarifa: string,
    descripcion:  string,
    costo: number,
    multaIncumplimiento: number,
    idUsuario: string,
    fchInicio: string,
    fchFin: string,
    estatus: string
}