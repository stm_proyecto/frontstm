
export interface Comite {

    idComite : number,
    descripcion : string,
    fechaAlta : string,
    fechaTermino : string,
    estatus : string,
    idUsuarioAlta : number
    
}