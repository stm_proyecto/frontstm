
export interface CobroColono {

    id: number,
    idColono:  number,
    numRecibo: string,
    descuento: string,
    razonDescuento: string,
    fchCargo: string,
    fchPago: string,
    estatus: string,
    idTarifa: number,
    montoCobrado: number,
    idUsuario: number,
    flgMulta: number
}