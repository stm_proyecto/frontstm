
export interface Colonos {
    nombreUsuario: string,
    apePa:  string,
    apeMa: string,
    estado: string,
    municipio: string,
    localidad: string,
    tipoZona: string,
    codigoPostal: string,
    calle: string,
    numeroExterior: string,
    numeroInterior: string,
    correo: string,
    telefono: string,
    identificacion: string,
    usuEsp: string,
    estatus: string,
    notas: string,
    fechaAlta: string,
    fechaBaja: string,
    idUsuario: number,
    idTarifa:number
}