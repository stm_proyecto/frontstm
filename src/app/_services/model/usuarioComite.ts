
export interface UsuarioComite {

    idUsuarioComite: number,
    idUsuario:  number,
    idComite: number,
    fechaAlta: string,
    idCargo: number,
    estatus: string
    
}