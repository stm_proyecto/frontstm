
export interface SalidaEfectivo {

    idSalidaEfectivo : number,
    descripcion:  string,
    cantidad: number,
    fechaAlta: string,
    idUsuario: number,
    fotoComprobante: string
    
}