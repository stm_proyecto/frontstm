import { Injectable } from '@angular/core';
import { NotificationService } from '../notificacion/notification.service';
@Injectable({
  providedIn: 'root'
})
export class ValidacionService {

  constructor(private _notificationService: NotificationService) { }

  /**
   * Regresa false y un mensaje si el campo esta vacio
   * @param objeto objeto [{campo: valor, mensaje: valor}]
   */
  vacioMensaje(objeto: any): boolean {
    let valida = true;
    objeto.forEach(element => {
      if (!element.campo) {
        this._notificationService.showWarning("Validacion", element.mensaje);
        valida = false;
      }
    });
    return valida;
  }

}
