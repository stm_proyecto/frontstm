import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as global from './variablesGlobales';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }) 
};

@Injectable({
    providedIn: 'root'
})

export class ReportesService {
    
    constructor(private http: HttpClient) { }

    reporteColonos(): Observable<any> {
        return this.http.get( global.urlServidor + '/reportes/reporteColonos');
      }

    reporteSalidaEfectivoMes(mesAnio): Observable<any> {
        return this.http.post( global.urlServidor + '/reportes/reporteSalidaEfectivoMes', mesAnio , httpOptions);
      } 
    
    reporteColonosInactivos(motivo): Observable<any> {
        return this.http.post( global.urlServidor + '/reportes/reporteColonosInactivos', motivo , httpOptions);
      } 

      reporteCobranzaMes(mesAnio): Observable<any> {
        return this.http.post( global.urlServidor + '/reportes/reporteCobranzaMes', mesAnio , httpOptions);
      }

      reporteColonosPendientes(): Observable<any> {
        return this.http.get( global.urlServidor + '/reportes/reporteColonosAdeudosAtrasados');
      }
}