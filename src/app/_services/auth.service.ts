import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as global from './variablesGlobales';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post( global.urlServidor + '/api/auth/signin', {
      correo: credentials.email,
      pass: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post( global.urlServidor + '/api/auth/signup', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }

  registro(user): Observable<any> {
    return this.http.post( global.urlServidor + '/api/auth/signup/registro', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }

  registroUsuarioI(user): Observable<any> {
    return this.http.post( global.urlServidor + '/api/auth/signup',user, httpOptions);
  }

}
