import { RouterModule } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { FuseSharedModule } from '../../../../../@fuse/shared.module';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { UsuarioExternoComponent } from './usuario-externo.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
//import { MatProgressButtonsModule } from 'mat-progress-buttons';

const routes = [
  {
      path     : 'auth/registerExterno',
      component: UsuarioExternoComponent
  }
];

@NgModule({
  declarations: [
    UsuarioExternoComponent
  ],
  imports     : [
      RouterModule.forChild(routes),

      MatButtonModule,
      MatCheckboxModule,
      MatFormFieldModule,
      MatIconModule,
      MatInputModule,

      FuseSharedModule,

      MatProgressSpinnerModule,
      MatProgressBarModule,
      //MatProgressButtonsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class UsuarioExternoModule{

}