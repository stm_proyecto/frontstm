import { Component, OnInit, OnDestroy, ViewEncapsulation ,Input} from '@angular/core';
import { Subject } from 'rxjs';
import { FuseConfigService } from '../../../../../@fuse/services/config.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '../../../../../@fuse/animations';
import { AuthService } from 'app/_services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import {ParametrosRegistro} from './../../../../../app/_services/model/paramRegistro';
import { Router } from '@angular/router';
import { TokenStorageService } from 'app/_services/token-storage.service';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';

//import { MatProgressButtonOptions } from 'mat-progress-buttons'

@Component({
  selector: 'usuario-externo',
  templateUrl: './usuario-externo.component.html',
  styleUrls: ['./usuario-externo.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class UsuarioExternoComponent implements OnInit, OnDestroy{

    color: ThemePalette = 'accent';
  mode: ProgressSpinnerMode = 'indeterminate';
  loading:boolean;

  confirma:any;

  registerForm: FormGroup;

  //parametros registro
  parametros : ParametrosRegistro;
  
  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private _AuthService:AuthService,
    private router: Router,
    private tokenStorage: TokenStorageService
)
{
    // Configure the layout
    this._fuseConfigService.config = {
        layout: {
            navbar   : {
                hidden: true
            },
            toolbar  : {
                hidden: true
            },
            footer   : {
                hidden: true
            },
            sidepanel: {
                hidden: true
            }
        }
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
}

// -----------------------------------------------------------------------------------------------------
// @ Lifecycle hooks
// -----------------------------------------------------------------------------------------------------

/**
 * On init
 */
ngOnInit(): void
{


    this.registerForm = this._formBuilder.group({
        name           : ['', Validators.required],
        email          : ['', [Validators.required, Validators.email]],
        password       : ['', Validators.required],
        passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
    });

    // Update the validity of the 'passwordConfirm' field
    // when the 'password' field changes
    this.registerForm.get('password').valueChanges
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
            this.registerForm.get('passwordConfirm').updateValueAndValidity();
        });

        this.parametros = {
          
            username : '',
            email : '',
            password : ''
    
        };

        this.loading = false;
}

/**
 * On destroy
 */
ngOnDestroy(): void
{
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
}


 


    registrar(){

        //console.log(this.parametros);
        this.loading = true;

        this.confirma = this.parametros.email;
        console.log(  this.confirma );
        this.tokenStorage.saveCorreo(this.confirma );
        
          this._AuthService.registro( this.parametros ).subscribe(
            data => {

              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'EL USUARIO SE REGISTRO CORRECTAMENTE!!!',
                showConfirmButton: false,
                timer: 2000
              })
              this.loading = false;
              this.pantallaConfirma();
         
            },
            err => {

              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'ERROR, AL REGISTRAR USUARIO!!!' ,
                showConfirmButton: false,
                timer: 2000
              })
              this.loading = false;
            }
          );
    }

    pantallaConfirma(){
        this.router.navigate(['/pages/auth/mail-confirm']);  // define your component where you want to go
    }

}


/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if ( !control.parent || !control )
  {
      return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if ( !password || !passwordConfirm )
  {
      return null;
  }

  if ( passwordConfirm.value === '' )
  {
      return null;
  }

  if ( password.value === passwordConfirm.value )
  {
      return null;
  }

  return {passwordsNotMatching: true};
};

