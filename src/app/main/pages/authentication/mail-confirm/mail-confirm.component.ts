import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { UsuarioExternoComponent } from '../usuario-externo/usuario-externo.component';
import { TokenStorageService } from 'app/_services/token-storage.service';

@Component({
    selector     : 'mail-confirm',
    templateUrl  : './mail-confirm.component.html',
    styleUrls    : ['./mail-confirm.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class MailConfirmComponent
{
    getCorreo:any;

    correoConfirma = this.tokenStorage.getCorreo();

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private tokenStorage: TokenStorageService
    )
    {
        //this.prueba = "prueba correo";
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

        /**
     * On init
     */
    ngOnInit(): void
    {
        this.getCorreo = this.correoConfirma;

    }

}
