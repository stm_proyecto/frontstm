import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { TokenStorageService } from 'app/_services/token-storage.service';
import { AuthService } from 'app/_services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
    selector     : 'login-2',
    templateUrl  : './login-2.component.html',
    styleUrls    : ['./login-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class Login2Component implements OnInit
{
    data:any;
    loginForm: FormGroup;
    isLoggedIn = false;
    isLoginFailed = false;
    errorMessage = '';
    roles: string[] = [];
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(private authService: AuthService, private tokenStorage: TokenStorageService,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder, private router: Router
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        if (this.tokenStorage.getToken()) {
            this.isLoggedIn = true;
            this.roles = this.tokenStorage.getUser().roles;
          }

        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.data = {
            email: '',
            password:'',
    
          };
      
    }
    loginAuth(): void {

        this.authService.login(this.data).subscribe(
          data => {

            if(data.valor == 0){

                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: data.mensaje ,
                    showConfirmButton: false,
                    timer: 2000
                  })

            }else if(data.valor == 1){

                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: data.mensaje ,
                    showConfirmButton: false,
                    timer: 2000
                  })

            }else {

                this.tokenStorage.saveToken(data.token);
                this.tokenStorage.saveUser(data);
        
                this.isLoginFailed = false;
                this.isLoggedIn = true;
                this.roles = this.tokenStorage.getUser().roles;
               this.router.navigate(['/pages'])

            }
           
          },
          err => {

            this.errorMessage = err.error.message;
            this.isLoginFailed = true;

            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'ERROR, AL INICIAR SESION!!!' ,
                showConfirmButton: false,
                timer: 2000
              })
          }
        );

      
          
      }
      reloadPage(): void {
        window.location.reload();
      }

}
