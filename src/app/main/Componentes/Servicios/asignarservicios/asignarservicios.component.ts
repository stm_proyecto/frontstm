import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {NotificationService} from '../../../../_services/notificacion/notification.service';

import { AsignarservicioService } from '../../../../_services/asignarservicio.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-asignarservicios',
  templateUrl: './asignarservicios.component.html',
  styleUrls: ['./asignarservicios.component.scss']
}) 

export class AsignarserviciosComponent{

  formGroup: FormGroup;

  catEmpleado: any[];
  requestEmpleado: any;

  catServicio: any[];
  requestServicio: any;

  catAreaServicio: any[];
  requestAreaServicio: any;

  boton:boolean;
  passConf :any;

  constructor(
    private _notifyService: NotificationService,

    private asignarServicioService: AsignarservicioService,
    private formBuilder: FormBuilder
  ) 
  {  }

  ngOnInit(): void {

    this.formGroup = this.formBuilder.group({
      empleadoId: ['', Validators.required],
      servicioId: ['', Validators.required],
      areaServicioId: ['', Validators.required],
      descripcion: ['', Validators.required]
    });

  this.getEmpleado();
  this.getServicio();
  this.getAreaServicio();
  }

  getEmpleado() {
    this.asignarServicioService.ListarEmpleado(this.requestEmpleado)
      .subscribe(
        (datos:any) => {
              this.catEmpleado = datos.lista;
              console.log(datos.lista)  
        },
        error => {
          this._notifyService.showError("Error", "Recuperación Empleado");
          console.log('Error : Recuperación Empleado' + error);
        });
  }

  getServicio() {
    this.asignarServicioService.ListarServicio(this.requestServicio)
      .subscribe(
        (datos:any) => {
              this.catServicio = datos.lista;
              console.log(datos.lista)  
        },
        error => {
          this._notifyService.showError("Error", "Recuperación Servicio");
          console.log('Error : Recuperación Servicio' + error);
        });
  }

  getAreaServicio() {
    this.asignarServicioService.ListarAreaServicio(this.requestAreaServicio)
      .subscribe(
        (datos:any) => {
              this.catAreaServicio = datos.lista;
              console.log(datos.lista)  
        },
        error => {
          this._notifyService.showError("Error", "Recuperación AreaServicio");
          console.log('Error : Recuperación AreaServicio' + error);
        });
  }


  asignarServicio() {
    let turno = {
      folio: 0, 
      descripcion: this.formGroup.get('descripcion').value,
      idServicio: {
        idServicio: this.formGroup.get('servicioId').value
      },
      idArea: {
        idArea: this.formGroup.get('areaServicioId').value
      },
      idEmpleado: {
        idEmpleado: this.formGroup.get('empleadoId').value
      }
    };
    console.log(turno);
    this.asignarServicioService.guardarTurno(turno).subscribe(
      (datos: any) => {
            this._notifyService.showSuccess("Mensaje", "El Turno fue asignado exitosamente.");
            console.log(datos); 
      },
      error => {
        this._notifyService.showError("Error", "El Turno no fue asignado");
        console.log('Error : El Turno no fue asignado: ' + error);
      });
  }

  get empleadoId() { return this.formGroup.get('empleadoId'); }
  get servicioId() { return this.formGroup.get('servicioId'); }
  get areaServicioId() { return this.formGroup.get('areaServicioId'); }
  get descripcion() { return this.formGroup.get('descripcion'); }

}
