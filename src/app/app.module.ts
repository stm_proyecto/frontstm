import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { Login2Component } from './main/pages/authentication/login-2/login-2.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { AdminGuard } from  './admin/admin.guard';

import {ComponentesModule} from './main/Componentes/Componentes.module';
import { UsuarioExternoModule } from './main/pages/authentication/usuario-externo/usuario-externo.module';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NotificationService } from './_services/notificacion/notification.service';
import { ValidacionService } from './_services/validacion/validacion.service';

import { ToastrModule } from 'ngx-toastr';
import { UsuarioExternoComponent } from './main/pages/authentication/usuario-externo/usuario-externo.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { UploadFileService } from './_services/upload-file.service';
import { Globals } from './_services/globals';

const appRoutes: Routes = [
   {
        path      : 'login', component:Login2Component
    },
    {
        path        : 'chat',
        loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule),canActivate: [AdminGuard]
    },
    {
        path        : 'pages',
        loadChildren: () => import('./main/pages/pages.module').then(m => m.PagesModule)
    },
    { path: '**', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        ToastrModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material

    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,

    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,

    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,

    MatPaginatorModule,

    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    

    MatTooltipModule,


        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        ComponentesModule,
        UsuarioExternoModule
        
    ],
    providers: [authInterceptorProviders,    NotificationService,
        ValidacionService,UploadFileService,Globals],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
