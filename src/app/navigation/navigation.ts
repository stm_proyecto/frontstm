import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'OPCIONES DEL SISTEMA',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        icon: 'apps',
        children: [
            {
             id: 'servicios',
                title: 'Servicio',
                translate: 'NAV.APPLICATIONS',
                type: 'collapsable',
                icon     : 'account_circle',
                children: [
                    {
                        id: 'asignarservicios',
                        title: 'Asignar Servicio',
                        type: 'item',
                        url: '/asignarservicios'
                    },
                    {
                        id: 'consultaUsuariosInfo',
                        title: 'Consultar Servicios',
                        type: 'item',
                        url: '/consultaUsuariosInfo'
                    }
                ]
            }

         ]
    }
];